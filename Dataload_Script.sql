--Table Import_Load_Details
CREATE TABLE [Vision Quest].[dbo].[Import_Load_Details](
	[LoadId] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[LoadName] [varchar](200) NULL,
	[DestinationDB] [varchar](100) NULL,
	[DestinationTbl] [varchar](100) NULL,
	[LoadStatus] [varchar](20) NULL,
	[LoadCount] [bigint] NULL,
	[StartDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[LoadType] [varchar](10) NULL,
	[SourceFile] [varchar](100) NULL,
	[SourceDB] [varchar](100) NULL,
	[SourceTbl] [varchar](100) NULL,
	[SourceQuery] [varchar](max) NULL,
)

--insert into [dbo].[Import_Load_Details] values('ABC','DEST1','TABLE1','FAILURE',0,'2014-08-14 10:01:08.46','2014-08-14 10:01:24.82','FILE','ABC.TXT',NULL,NULL,NULL)
--insert into [dbo].[Import_Load_Details] values('DEF','DEST1','TABLE2','SUCCESS',199847,DATEADD(MINUTE,2,getdate()),DATEADD(MINUTE,6,getdate()),'FILE','DEF.TXT',NULL,NULL,NULL)
--insert into [dbo].[Import_Load_Details] values('ASDF','DEST2','TABLE1','SUCCESS',52112,DATEADD(MINUTE,3,getdate()),DATEADD(MINUTE,5,getdate()),'DB',NULL,'SRCDB1','SRCTB1','SELECT * FROM SRCTB1')
--insert into [dbo].[Import_Load_Details] values('CCDF','DEST3','TABLE7','SUCCESS',377896,DATEADD(MINUTE,2,getdate()),DATEADD(MINUTE,9,getdate()),'FILE','MNO.CSV',NULL,NULL,NULL)
--insert into [dbo].[Import_Load_Details] values('VBHG','DEST1','TABLE5','SUCCESS',28962,DATEADD(MINUTE,4,getdate()),DATEADD(MINUTE,5,getdate()),'FILE','BFGY.TXT',NULL,NULL,NULL)



SELECT TOP 1000 [LoadId]
      ,[LoadName]
      ,[DestinationDB]
      ,[DestinationTbl]
      ,[LoadStatus]
      ,[LoadCount]
      ,[StartDateTime]
      ,[EndDateTime]
      ,[LoadType]
      ,[SourceFile]
      ,[SourceDB]
      ,[SourceTbl]
      ,[SourceQuery]
  FROM [Vision Quest].[dbo].[Import_Load_Details]
  
  
update Import_Load_Details set LoadCount=0,LoadStatus='SUCCESS',StartDateTime=DATEADD(MINUTE,1375,getdate()),EndDateTime=DATEADD(MINUTE,1375,getdate()) where LoadId=5



truncate table [Vision Quest].[dbo].[Import_Load_Details]

--Table Import_Load_Details_Hist

CREATE TABLE [Vision Quest].[dbo].[Import_Load_Details_Hist](
	[Id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[LoadId] [bigint] NOT NULL,
	[LoadName] [varchar](200) NULL,
	[DestinationDB] [varchar](100) NULL,
	[DestinationTbl] [varchar](100) NULL,
	[LoadStatus] [varchar](20) NULL,
	[LoadCount] [bigint] NULL,
	[StartDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[LoadType] [varchar](10) NULL,
	[SourceFile] [varchar](100) NULL,
	[SourceDB] [varchar](100) NULL,
	[SourceTbl] [varchar](100) NULL,
	[SourceQuery] [varchar](max) NULL,
)

SELECT TOP 1000 [Id]
      ,[LoadId]
      ,[LoadName]
      ,[DestinationDB]
      ,[DestinationTbl]
      ,[LoadStatus]
      ,[LoadCount]
      ,[StartDateTime]
      ,[EndDateTime]
      ,[LoadType]
      ,[SourceFile]
      ,[SourceDB]
      ,[SourceTbl]
      ,[SourceQuery]
  FROM [Vision Quest].[dbo].[Import_Load_Details_Hist]


  truncate table [Vision Quest].[dbo].[Import_Load_Details_Hist]


--Table Import_Load_Error

CREATE TABLE [Vision Quest].[dbo].[Import_Load_Error](
	[JobName] [varchar] (255),
	[LogDate] [varchar] (max),
	[Error] [varchar] (max)
)

SELECT TOP 1000 [JobName]
      ,[LogDate]
      ,[Error]
  FROM [Vision Quest].[dbo].[Import_Load_Error]
  
    
  	Truncate table [Vision Quest].[dbo].[Import_Load_Error]



--SSIS Packages

--DataLoadHistory.dtsx

	--Query
	select * FROM Import_Load_Details ILD WHERE LoadName NOT IN (SELECT LoadName FROM Import_Load_Details_Hist)--Insert New
	Union
	select distinct ILD.*
	from Import_Load_Details ILD inner join Import_Load_Details_Hist ILDH
	on ILD.LoadName=ILDH.LoadName
	where ILD.StartDateTime>(select MAX(EndDateTime) from Import_Load_Details_Hist where LoadName=ILDH.loadname)--Update Existing
	
	--Job: JOB_DataLoadHistory
	
	USE [msdb]
	GO

	/****** Object:  Job [JOB_DataLoadHistory]    Script Date: 11/13/2014 11:04:30 ******/
	BEGIN TRANSACTION
	DECLARE @ReturnCode INT
	SELECT @ReturnCode = 0
	/****** Object:  JobCategory [Database Engine Tuning Advisor]    Script Date: 11/13/2014 11:04:30 ******/
	IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Engine Tuning Advisor' AND category_class=1)
	BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Engine Tuning Advisor'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	
	END
	
	DECLARE @jobId BINARY(16)
	EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'JOB_DataLoadHistory', 
			@enabled=1, 
			@notify_level_eventlog=0, 
			@notify_level_email=0, 
			@notify_level_netsend=0, 
			@notify_level_page=0, 
			@delete_level=0, 
			@description=N'to load data into history table', 
			@category_name=N'Database Engine Tuning Advisor', 
			@owner_login_name=N'Avinash-LAPTOP\Avinash', @job_id = @jobId OUTPUT
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	/****** Object:  Step [load data]    Script Date: 11/13/2014 11:04:30 ******/
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'load data', 
			@step_id=1, 
			@cmdexec_success_code=0, 
			@on_success_action=1, 
			@on_success_step_id=0, 
			@on_fail_action=2, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, @subsystem=N'SSIS', 
			@command=N'/DTS "\MSDB\DataLoadHistory" /SERVER "AVINASH-LAPTOP" /CHECKPOINTING OFF /REPORTING E', 
			@database_name=N'master', 
			@flags=0
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'SCHED_Data_Load', 
			@enabled=1, 
			@freq_type=4, 
			@freq_interval=1, 
			@freq_subday_type=1, 
			@freq_subday_interval=0, 
			@freq_relative_interval=0, 
			@freq_recurrence_factor=0, 
			@active_start_date=20141110, 
			@active_end_date=99991231, 
			@active_start_time=191000, 
			@active_end_time=235959, 
			@schedule_uid=N'c33aefe2-e288-4916-85cc-5296f84328ca'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	COMMIT TRANSACTION
	GOTO EndSave
	QuitWithRollback:
	    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
	EndSave:
	
	GO

--ErrorLogToTable.dtsx
	
	--Job: JOB_ErrorLogToTable
	USE [msdb]
	GO
	
	/****** Object:  Job [JOB_ErrorLogToTable]    Script Date: 11/13/2014 11:08:42 ******/
	BEGIN TRANSACTION
	DECLARE @ReturnCode INT
	SELECT @ReturnCode = 0
	/****** Object:  JobCategory [Database Engine Tuning Advisor]    Script Date: 11/13/2014 11:08:42 ******/
	IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Engine Tuning Advisor' AND category_class=1)
	BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Engine Tuning Advisor'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	
	END
	
	DECLARE @jobId BINARY(16)
	EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'JOB_ErrorLogToTable', 
			@enabled=1, 
			@notify_level_eventlog=0, 
			@notify_level_email=0, 
			@notify_level_netsend=0, 
			@notify_level_page=0, 
			@delete_level=0, 
			@description=N'No description available.', 
			@category_name=N'Database Engine Tuning Advisor', 
			@owner_login_name=N'Avinash-LAPTOP\Avinash', @job_id = @jobId OUTPUT
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	/****** Object:  Step [LogToTable]    Script Date: 11/13/2014 11:08:42 ******/
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'LogToTable', 
			@step_id=1, 
			@cmdexec_success_code=0, 
			@on_success_action=1, 
			@on_success_step_id=0, 
			@on_fail_action=2, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, @subsystem=N'SSIS', 
			@command=N'/DTS "\MSDB\ErrorLogToTable" /SERVER "AVINASH-LAPTOP" /CHECKPOINTING OFF /REPORTING E', 
			@database_name=N'master', 
			@flags=0
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'SCHED_Data_Load', 
			@enabled=1, 
			@freq_type=4, 
			@freq_interval=1, 
			@freq_subday_type=1, 
			@freq_subday_interval=0, 
			@freq_relative_interval=0, 
			@freq_recurrence_factor=0, 
			@active_start_date=20141110, 
			@active_end_date=99991231, 
			@active_start_time=191000, 
			@active_end_time=235959, 
			@schedule_uid=N'c33aefe2-e288-4916-85cc-5296f84328ca'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	COMMIT TRANSACTION
	GOTO EndSave
	QuitWithRollback:
	    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
	EndSave:
	
	GO



--Testing Queries
  delete from Import_Load_Details_Hist where ID in (9,10)

--Report Query
	--PARAM_Job_Name
	--Dataset for JobNameParam Parameter
	select distinct loadname from Import_Load_Details_HIST
	
	--DS_Load_Job_Intervals
	--Dataset for Load_Job_Intervals Chart
	select loadname,LoadCount,StartDateTime,EndDateTime,DATEDIFF(MINUTE,StartDateTime,EndDateTime) as Time_Elapsed 
	from Import_Load_Details_Hist 
	where LoadName in (@JobNameParam)
	and CONVERT(VARCHAR(10),StartDateTime,101) between @From_Date and @To_Date
	
	--DS_Job_Success
	--Dataset for Job_Success Chart
	select loadname,LoadCount,StartDateTime,EndDateTime,LoadStatus
	from Import_Load_Details_Hist 
	where LoadName in (@JobNameParam)
	and CONVERT(VARCHAR(10),StartDateTime,101) between @From_Date and @To_Date
	
